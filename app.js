document.querySelector('#tipForm').onchange = function(){
    const totalBill = Number(document.querySelector('#totalBill').value);
    const tipInput = document.querySelector('#tipInput').value;
    const tipOutput = document.querySelector('#tipOutput');
    const tipAmount = document.querySelector('#tipAmount');
    const totalBillWithTip = document.querySelector('#totalBillWithTip');
    const results = document.querySelector('#results');

    const tipValue = totalBill * ( tipInput/100);
    const finalBill = totalBill + tipValue;

    tipAmount.value = tipValue.toFixed(2);
    totalBillWithTip.value = finalBill.toFixed(2);

    tipOutput.innerHTML= `${tipInput}`;
    results.style.display='block';

}

